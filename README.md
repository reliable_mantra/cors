# Cross-Origin Resource Sharing (CORS)

Demonstration of the CORS concept. Run the app and make fetch requests within the chrome developer tools console from any other domain.

## No 'Access-Control-Allow-Origin' header
Request from https://www.google.com:
```
fetch("http://localhost:8888/").then(a => a.text()).then(console.log)
```

Response:  
Access to fetch at 'http://localhost:8888/' from origin 'https://www.google.com' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. 
If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.

So we have successfully reached the resource but the browser denied the response.

## The 'Access-Control-Allow-Origin' header has a value 'https://www.google.com' that is not equal to the supplied origin.
Request from https://www.yahoo.com:
```
fetch("http://localhost:8888/").then(a => a.text()).then(console.log)
```

Response:  
Access to fetch at 'http://localhost:8888/' from origin 'https://www.yahoo.com' has been blocked by CORS policy: The 'Access-Control-Allow-Origin' header has a value 'https://www.google.com' that is not equal to the supplied origin. 
Have the server send the header with a valid value, or, if an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.

## Response to preflight request doesn't pass access control check
Request from https://www.google.com:
```
fetch("http://localhost:8888/", {headers: {"Content-Type": "json/application"}}).then(a => a.text()).then(console.log)
```

Resoponse:  
Access to fetch at 'http://localhost:8888/' from origin 'https://www.google.com' has been blocked by CORS policy: Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource. 
If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.

The preflight options request protects unsuspecting servers from receiving cross-origin requests they may not want. 
A preflight request is issued when a request meets any of the following criteria:

* It uses an HTTP method other than GET, POST, or HEAD  
* It sets the Content-Type request header with values other than:
    * `application/x-www-form-urlencoded`
    * `multipart/form-data`
    * `text/plain`  
* It sets additional request headers that are not:
    * Accept
    * Accept-Language
    * Content-Language  
* The XMLHttpRequest contains upload events

# Request header field content-type is not allowed by Access-Control-Allow-Headers in preflight response
Request from https://www.google.com:
```
fetch("http://localhost:8888/", {headers: {"Content-Type": "json/application"}}).then(a => a.text()).then(console.log)
```

Resoponse:  
Access to fetch at 'http://localhost:8888/' from origin 'https://www.google.com' has been blocked by CORS policy: Request header field content-type is not allowed by Access-Control-Allow-Headers in preflight response.
