from flask import Flask, request, make_response

app = Flask(__name__)

@app.route('/', methods=['GET', 'OPTIONS'])
def hello_world():
    if request.method == 'OPTIONS':
        response = make_response('')
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        return response, 204

    response = make_response('Hello, World!')
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888, debug=True)
